/*
    cookietool is (c) 1995-2001 by Wilhelm Noeker (wnoeker@t-online.de)

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA
    
 */

/*========================================================================*\
 |  File: strstuff.h                                   Date: 20 Sep 1997  |
 *------------------------------------------------------------------------*
 |   Some string functions, similar to those from the standard library    |
 |                  in <string.h>, but more flexible.                     |
 |   You must call str_setup() before using any of these functions !!!    |
 |                                                                        |
\*========================================================================*/

#ifndef _STRSTUFF_H_
#define _STRSTUFF_H_

typedef unsigned char UBYTE;

/* special return values for str_cmp() and strn_cmp() */
#define STR_LONGER   256   
#define STR_SHORTER -256

/* possible values for bordermode (treatment of word delimiters): */
#define BM_FUSSY       3  /* compare char by char */
#define BM_DONTCOUNT   2  /* number (and kind) of spaces doesn't matter */
#define BM_SPACEPUNCTS 1  /* treat punctuation like spaces */
#define BM_MERGE       0  /* merge all alphanumerics into one single word */

void   str_setup    ( int bordermode, int case_sense );
void   print_strstat( void );
int    str_cmp      ( UBYTE *s, UBYTE *t );
int    str_ncmp     ( UBYTE *s, UBYTE *t, size_t n );
UBYTE *str_str      ( UBYTE *s, UBYTE *t );
UBYTE *str_ncpy     ( UBYTE *t, UBYTE *s, size_t n );

#endif

