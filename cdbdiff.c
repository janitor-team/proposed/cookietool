/*
    cdbdiff is (c) 2001 by Wilhelm Noeker (wnoeker@t-online.de)

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 2 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
    02111-1307 USA

 */


/*========================================================================*\
 |  File: cdbdiff.c                                    Date: 22 Mar 2001  |
 *------------------------------------------------------------------------*
 |             Compare two fortune cookie files 'A' and 'B',              |
 |    extracting all cookies which are present in 'B' but not in 'A'.     |
 |                     See help() for usage notes.                        |
 |                                                                        |
\*========================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "strstuff.h"
#include "cookio.h"
#include "compress.h"

char version[] = "$VER: cdbdiff 2.5 (22.03.2001)";


#define FBUFSIZE 16384          /* we'll use larger file buffers */



/*
 * Print a help text and nag about illegal parameter <s>
 */
void help( UBYTE *s )
    {
    if( s )
        printf( "illegal option '%s'\n", s );
    printf( "usage:  cdbdiff [options] <dbase0> <dbase1> <resultfile>\n" );
    printf( "where options are:\n" );
    printf( " -f[0-3] cookie format: words, lines, %%, %%%% (default: %d)\n", DEF_FMT );
    printf( " -d[0-3] how fussy about word delimiters? (default: 2)\n" );
    printf( " -c      case sensitive comparisons\n" );
    printf( " -a      if <resultfile> exists, append to it\n" );
    }



int main( int argc, char *argv[] )
    {
    UBYTE *s;
    int append = 0;
    int case_sense = 0, bordermode = 2;
    int fmt = DEF_FMT;
    long count1;
    UBYTE name1[ 100 ], name2[ 100 ], name3[ 100 ];
    UBYTE hooktarget[ 16 ];
    FILE *infile, *infile2, *logfile;

    name1[ 0 ] = name2[ 0 ] = name3[ 0 ] = '\0';
    while( --argc )
        {
        s = *++argv;
        if( *s != '-' )
            {
            if( name1[ 0 ] == '\0' )
                strcpy( name1, s );
            else if( name2[ 0 ] == '\0' )
                strcpy( name2, s );
            else
                strcpy( name3, s );
            }
        else
            {
            s++;
            switch( *s++ )
                {
                case 'd':
                    if( isdigit( *s ) )
                        bordermode = atoi( s );
                    else
                        goto bad_parm;
                    break;
                case 'f':
                    if( isdigit( *s ) )
                        fmt = atoi( s );
                    else
                        goto bad_parm;
                    break;
                case 'c':
                    case_sense = 1;
                    break;
                case 'a':
                    append = 1;
                    break;
                default:
                    goto bad_parm;
                }
            }
        }
    /* important, before calling anything from strstuff: */
    str_setup( bordermode, case_sense );
    if( *name3 == '\0' )
        {
        help( NULL );
        return 5;
        }
    printf( "CdbDiff " );
    print_strstat();
    printf( "File format: " );
    print_fmtinfo( fmt );
    /* Open all three files. */
    if( !(infile = fopen( name1, "r" ) ) )
        {
        printf( "Can't open %s for input!\n", name1 );
        return 10;
        }
    if( !(infile2 = fopen( name2, "r" ) ) )
        {
        printf( "Can't open %s for input!\n", name2 );
        return 10;
        }
    if( !append && (logfile = fopen( name3, "r" )) )
        {
        printf( "Error: '%s' exists!  Use -a to append.\n", name3 );
        return 10;
        }
    if( !(logfile = fopen( name3, "a" ) ) )
        {
        printf( "Can't open %s for output!\n", name3 );
        return 10;
        }
    setvbuf( infile, NULL, _IOFBF, FBUFSIZE );
    setvbuf( infile2, NULL, _IOFBF, FBUFSIZE );
    setvbuf( logfile, NULL, _IOFBF, FBUFSIZE );
    /* Read the first cookie file and compress it. */
    read_cookies( infile, fmt );
    fclose( infile );
    one_cookie( DUPDEL_MATCH, SORT_RESTORE, hooktarget, NULL, fmt );
    count1 = listed;
    /* Add the second file and once more delete any duplicates. */
    read_cookies( infile2, fmt );
    fclose( infile2 );
    one_cookie( DUPDEL_MATCH, SORT_RESTORE, hooktarget, NULL, fmt );
    /* Write the remaining cookies, skipping those from the first file. */
    write_cookies( logfile, fmt, count1 );
    fclose( logfile );
    return 0;
bad_parm:
    help( argv[ 0 ] );
    return 5;
    }
